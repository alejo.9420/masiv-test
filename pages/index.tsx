import Head from 'next/head';
import React, { useState } from 'react';
import { useSnackbar } from 'react-simple-snackbar';
import styles from '../styles/pages/Home.module.scss';
import axios from 'axios';
import Icon from '@components/icon';
import Tags from '@components/tags';
import Button from '@components/button';
import { getRandomArbitrary } from 'utils/math';

type Props = {
  alt: string;
  day: string | number;
  img: string;
  link: string;
  month: string | number;
  news: string;
  num: number;
  safe_title: string;
  title: string;
  transcript: string;
  year: string | number;
};

const Home = ({ title, img, alt, year, link, month, day, ...rest }: Props) => {
  const formatedTitle = `${title} - (${year}-${month}-${day})`;
  const [rate, setRate] = useState(0);
  const [openSnackbar] = useSnackbar();

  const handleRateClick = (rate: number) => {
    setRate(rate);
    // Expected message after fake rate update is successfully
    openSnackbar(`Su calificación de ${title} ha sido enviada.`);
  };

  const handleUpdateComic = () => {
    window.location.reload();
  };

  return (
    <>
      <Head>
        <title>{formatedTitle}</title>
      </Head>

      <div className={styles.home}>
        <div className={styles.home__header}>
          <h1 className={styles['home__header-title']}>
            Calificación de comics
          </h1>
        </div>
        <div className={styles.home__container}>
          <div className={styles.home__flex}>
            <Button onClick={handleUpdateComic} className={styles.home__button}>
              Actualizar
            </Button>

            <div className={styles['home__comic-container']}>
              <h3 className={styles['home__comic-title']}>{formatedTitle}</h3>
              <img className={styles.home__img} src={img} alt={alt} />

              <div style={{ marginTop: 8 }}>
                <Tags
                  tags={[
                    'Bélico',
                    'Humor',
                    'Deportivo',
                    'Aventuras',
                    'Ciencia ficción',
                    'Histórico',
                    'Policíaco',
                    'Romantico',
                    'Terror',
                  ]}
                />
              </div>
            </div>

            <p>{rest.transcript}</p>

            <div style={{ textAlign: 'center' }}>
              <Icon
                onClick={() => handleRateClick(1)}
                className={styles.home__star}
                icon={rate >= 1 ? 'icon-star' : 'icon-star-o'}
              />
              <Icon
                onClick={() => handleRateClick(2)}
                className={styles.home__star}
                icon={rate >= 2 ? 'icon-star' : 'icon-star-o'}
              />
              <Icon
                onClick={() => handleRateClick(3)}
                className={styles.home__star}
                icon={rate >= 3 ? 'icon-star' : 'icon-star-o'}
              />
              <Icon
                onClick={() => handleRateClick(4)}
                className={styles.home__star}
                icon={rate >= 4 ? 'icon-star' : 'icon-star-o'}
              />
              <Icon
                onClick={() => handleRateClick(5)}
                className={styles.home__star}
                icon={rate >= 5 ? 'icon-star' : 'icon-star-o'}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

Home.getInitialProps = async (context) => {
  // El número maximo de comics fue encontrado probando el limite encontrado al cambiar el id del comic
  const randomId = getRandomArbitrary(1, 2454);
  const res = await axios.get(`https://xkcd.com/${randomId}/info.0.json`);
  const data = res.data;
  return { ...data };
};

export default Home;
