La prueba fue realizada con el framework [Next.js](https://nextjs.org/), usa React.js, tiene un enfoque para SSR y SEO.

## Getting Started

Instalar dependencias:

```bash
npm install
# o
yarn install
```

Para iniciar el servidor de desarrollo:

```bash
npm run dev
# o
yarn dev
```

Abrir [http://localhost:3000](http://localhost:3000) en el navegador para acceder a la app.
