const withPlugins = require('next-compose-plugins');
const withImages = require('next-images');

const nextConfig = {
  webpack(config) {
    config.resolve.alias['~'] = path.resolve(__dirname);
    config.module.rules.push({
      test: /\.(woff|woff2|eot|ttf|svg)$/,
      loader: 'url-loader?limit=100000',
    });
    return config;
  },
};

module.exports = withPlugins([withImages()], nextConfig);
