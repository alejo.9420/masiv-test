import React from 'react';

const Icon = ({ icon, className, ...rest }) => (
  <i {...rest} className={`${icon} ${className}`} />
);

export default Icon;
