import React, { useEffect, useState } from 'react';
import styles from '../styles/components/Tags.module.scss';

type Props = {
  tags: string[];
};

const Tags = ({ tags }: Props) => {
  const [internalTags, setInternalTags] = useState([]);

  useEffect(() => {
    const temp = [];

    for (const tag of tags) {
      temp.push({ tag, active: false });
    }

    setInternalTags(temp);
  }, []);

  const handleTagClick = (item) => {
    const index = internalTags.indexOf(item);
    internalTags[index].active = !item.active;
    setInternalTags([...internalTags]);
  };

  const getStyles = (item) => {
    return item.active
      ? styles['tags__tag--active']
      : styles['tags__tag--inactive'];
  };

  return (
    <div className={styles.tags}>
      {internalTags.map((item, i) => (
        <span
          key={i}
          className={`${styles.tags__tag} ${getStyles(item)}`}
          onClick={() => handleTagClick(item)}
        >
          {item.tag}
        </span>
      ))}
    </div>
  );
};

export default Tags;
