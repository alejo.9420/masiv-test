import React from 'react';
import styles from '../styles/components/Button.module.scss';

type Props = {
  className: string;
  children: JSX.Element | string;
  onClick: () => void;
};

const Button = ({ className, children, ...rest }: Props) => {
  return (
    <button {...rest} className={`${styles.button} ${className}`}>
      {children}
    </button>
  );
};

export default Button;
